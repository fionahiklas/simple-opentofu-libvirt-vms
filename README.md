# Simple Open Tofo Libvirt VMs

## Overview 


## Quickstart

* Follow the prerequisite steps
* Check the OpenTofu plan and then apply

```
tofu plan
tofu apply
```

* The guest VM's IP address will be output 
* Connect to console (force disconnect if there is one already present)

```
virsh console debian12 --force
```

* Or use SSH

```
ssh debian@<ip address returned from tofu>
```

* Use `virsh shutdown debian2` to shut the VM down
* Use `virsh start debian12` to start it again
* Use `tofu destroy` to remove all resources (the VM must be running)



## Prerequisites

### Libvirt and KVM

Installing using the following command

```
sudo apt install libvirt-daemon libvirt-clients libvirt-wireshark libvirt-daemon-driver-qemu qemu-kvm virtinst kvmtool libvirt-daemon-system virt-manager bridge-utils
```

__NOTE:__ It seems that the `qemu-kvm` package is actually called `qemu-system-x86` as I get this message when trying to install

```
Note, selecting 'qemu-system-x86' instead of 'qemu-kvm'
```

Ensure that the libvirt daemon is running

```
sudo systemctl start libvirtd
sudo systemctl enable libvirtd
```

Ensure the `vhost_net` module is added 

```
sudo modprobe vhost_net
echo vhost_net | sudo tee -a /etc/modules
```

Ensure that the users that need to create VMs are added to the `libvirt` group

```
sudo usermod -a -G libvirt fiona
```


### OpenTofu

Ideally I'd like to compile the latest code from the git repo using Go but Ubuntu 
22.04 only has Go 1.18 and that seems a little old now.  Sticking to the 
pre-built snap package

```
sudo snap install opentofu --classic
```


### Linux tools

Run to install some useful/needed tools

```
apt install apparmor-utils
```


### Setup QEMU storage pool

Create a default storage pool

```
# Create storage location - I'm using a logical volume mounted under
# /mnt/vms
mkdir -p /mnt/vms/libvirt/storage/default-pool

# Add pool to QEMU
virsh -c qemu:///session pool-define-as --name default --type dir --target $HOME/.libvirt/storage/default-pool
```

On Ubuntu (I'm running 22.04) you need to setup AppArmor

I edited the file `/etc/apparmor.d/local/abstractions/libvirt-qemu`

```
/mnt/vms/libvirt/** rwk,

```

The newline at the end of the file is significant (can't remember why)


## OpenTofu Init 

Run the following command from the top level of the cloned repo 

```
tofu init
```

This gives the following output 

```
Initializing the backend...

Initializing provider plugins...
- Finding latest version of dmacvicar/libvirt...
- Installing dmacvicar/libvirt v0.7.4...
- Installed dmacvicar/libvirt v0.7.4. Signature validation was skipped due to the registry not containing GPG keys for this provider

OpenTofu has created a lock file .terraform.lock.hcl to record the provider
selections it made above. Include this file in your version control repository
so that OpenTofu can guarantee to make the same selections by default when
you run "tofu init" in the future.

OpenTofu has been successfully initialized!

You may now begin working with OpenTofu. Try running "tofu plan" to see
any changes that are required for your infrastructure. All OpenTofu commands
should now work.

If you ever set or change modules or backend configuration for OpenTofu,
rerun this command to reinitialize your working directory. If you forget, other
commands will detect it and remind you to do so if necessary.
```



## References

### OpenTofu

* [Github Repos](https://github.com/opentofu)
* [Installing OpenTofu](https://opentofu.org/docs/intro/install)
* [Terraform block](https://opentofu.org/docs/language/settings/)
* [How to provision VMs with Terraform and KVM](https://computingforgeeks.com/how-to-provision-vms-on-kvm-with-terraform/?expand_article=1)
* [Libvirt handling network interfaces](https://registry.terraform.io/providers/multani/libvirt/latest/docs/resources/domain#handling-network-interfaces)
* [Terraform libvirt plugin cloud-init](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/cloudinit)
* [Libvirt plugin cloud init](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs/resources/cloudinit)
* [For loop](https://developer.hashicorp.com/terraform/language/expressions/for)
* [Yamlencode](https://developer.hashicorp.com/terraform/language/functions/yamlencode#yamlencode-function)
* [Template file](https://registry.terraform.io/providers/hashicorp/template/latest/docs/data-sources/file)
* [Template file function](https://developer.hashicorp.com/terraform/language/functions/templatefile)


### Libvirt

* [Libvirt overview](https://wiki.archlinux.org/title/Libvirt)
* [QEMU/KVM Hypervisor driver](https://libvirt.org/drvqemu.html)
* [How to install QEMU/KVM on Linux](https://www.tecmint.com/install-qemu-kvm-ubuntu-create-virtual-machines/)
* [Libvirt provider](https://registry.terraform.io/providers/dmacvicar/libvirt/latest/docs)
* [Creating storage pools](https://unix.stackexchange.com/questions/216837/virsh-and-creating-storage-pools-what-is-sourcepath)
* [Creating the default storage pool](https://stackoverflow.com/questions/70230848/kvm-virsh-the-storage-pool-is-empty)
* [List domains](https://unix.stackexchange.com/questions/300113/how-to-list-domains-in-virsh)
* [Cheatsheet](https://computingforgeeks.com/virsh-commands-cheatsheet/)
* [Remove VM](https://computingforgeeks.com/virsh-commands-cheatsheet/#14-virsh-remove-vm)
* [KVM and AppArmor](https://unix.stackexchange.com/questions/435837/how-to-configure-apparmor-so-that-kvm-can-start-guest-that-has-a-backing-file-ch)
* [QEMU system vs session](https://blog.wikichoon.com/2016/01/qemusystem-vs-qemusession.html)
* [libvirt connection URLs](https://libvirt.org/uri.html)
* [AppArmor for libvirt](https://www.turek.dev/posts/add-custom-libvirt-apparmor-permissions/)
* [Using virsh to dump XML](https://www.techtarget.com/searchitoperations/tip/Basic-virsh-network-commands-to-know)
* [Libvirt and AppArmor](https://askubuntu.com/questions/1159366/apparmor-permission-for-libvirt-qemu)
* [Libvirt daemons](https://libvirt.org/daemons.html)
* [Mount QCOW2 images](https://www.baeldung.com/linux/mount-qcow2-image)
* [virsh dump](https://www.ibm.com/docs/en/linux-on-z?topic=91-kvm-dump-virsh-dump)


### Debian

* [Debian Cloud/VM images](https://cloud.debian.org/images/cloud/)
* [Default Debian image passwords](https://www.reddit.com/r/debian/comments/107511p/default_userpassword_for_qcow_images/?rdt=50020)
* [Managing Debian images with cloud-init](https://philpep.org/blog/debian-vm-on-lvm-with-cloud-init/)
* [Cloud init with Debian images](https://sumit-ghosh.com/posts/create-vm-using-libvirt-cloud-images-cloud-init/)
* [Cloudinit docs for 22.4 matches Debian 12/bookworm](https://cloudinit.readthedocs.io/en/22.4/topics/modules.html#set-passwords)



### Linux

* [Modify user groups](https://linuxize.com/post/how-to-add-user-to-group-in-linux/)
* [Modify ACLs](https://unix.stackexchange.com/questions/12842/make-all-new-files-in-a-directory-accessible-to-a-group)
* [AppArmor on Ubuntu](https://wiki.ubuntu.com/AppArmor)
* [What does rwk AppArmor permission mean](https://stackoverflow.com/questions/23245086/what-does-rwk-stands-for)
* [Finding IP addresses using ip command](https://linuxopsys.com/topics/list-network-interfaces-in-linux)
* [Cloud init](https://cloudinit.readthedocs.io/en/latest/)
* [Cloud init boot stages](https://cloudinit.readthedocs.io/en/latest/explanation/boot.html)
* [Cloud init write files](https://cloudinit.readthedocs.io/en/latest/reference/modules.html#write-files)
* [Journalctl commands](https://linuxhandbook.com/journalctl-command/)
* [Force SSH password authentication](https://unix.stackexchange.com/questions/15138/how-to-force-ssh-client-to-use-only-password-auth)
* [User data cloud config files](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html#user-data-shell-scripts)


### Emacs

* [Adding variable mode line](https://www.gnu.org/software/emacs/manual/html_node/emacs/Specifying-File-Variables.html)








