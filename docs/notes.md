## Notes

### Intital apply

When first running `tofu apply`  I got this error

```
libvirt_volume.debian12-qcow2: Creating...
╷
│ Error: can't find storage pool 'default'
│ 
│   with libvirt_volume.debian12-qcow2,
│   on main.tf line 2, in resource "libvirt_volume" "debian12-qcow2":
│    2: resource "libvirt_volume" "debian12-qcow2" {
│ 
╵
```

There is a note in the code that says to run `virsh pool-list` got this 

```
virsh pool-list
 Name   State   Autostart
---------------------------
```

Running this command

```
virsh pool-define-as --name default --type dir --target /var/run/libvirt/storage/default-pool
```

NOTE: This seems to work for a normal user, I presume the daemon is doing the actual work

Now there is a pool visible

```
virsh pool-list --all
 Name      State      Autostart
---------------------------------
 default   inactive   no
```

Tried to start this pool but needed to actually create the directory

```
sudo mkdir /var/run/libvirt/storage/default-pool
```

Then ran the following 

```
virsh pool-start default
virsh pool-autostart default
virsh pool-list

Name      State    Autostart
-------------------------------
 default   active   yes
```


### Permission Denied in storage pool

After the above issue had another one

```
libvirt_volume.debian12-qcow2: Creating...
libvirt_volume.debian12-qcow2: Creation complete after 6s [id=/var/run/libvirt/storage/default-pool/debian12.qcow2]
libvirt_domain.debian12: Creating...
╷
│ Error: error creating libvirt domain: internal error: process exited while connecting to monitor: 2023-11-08T10:17:36.280399Z qemu-system-x86_64: -blockdev {"driver":"file","filename":"/var/run/libvirt/storage/default-pool/debian12.qcow2","node-name":"libvirt-1-storage","auto-read-only":true,"discard":"unmap"}: Could not open '/var/run/libvirt/storage/default-pool/debian12.qcow2': Permission denied
│ 
│   with libvirt_domain.debian12,
│   on main.tf line 11, in resource "libvirt_domain" "debian12":
│   11: resource "libvirt_domain" "debian12" {
│ 
╵
```

Deleted the `debian12.qcow2` that was already downloaded there and tried just changing 
the directory permission 

```
chgrp libvirt /var/run/libvirt/storage/default-pool/
```

Actually needed to run `tofu destory` since I got this error 

```
│ Error: error defining libvirt domain: operation failed: domain 'debian12' already exists with uuid 32f982fe-7932-4777-b0dc-54b0d8a37b2c
│
│   with libvirt_domain.debian12,
│   on main.tf line 11, in resource "libvirt_domain" "debian12":
│   11: resource "libvirt_domain" "debian12" {
│
╵
```

Even though I deleted the file the state said it already existed.  This 
worked, trying apply again.

Still getting a similar error

```
virsh list --all

 Id   Name       State
---------------------------
 -    debian12   shut off
```

Was able to remove with this command

```
virsh undefine debian12

virsh list --all

Id   Name   State
--------------------
```

Trying to use ACLs to sort this issue

```
setfacl -d -m group:libvirt:rwx /var/run/libvirt/storage/default-pool
setfacl -m group:libvirt:rw /var/run/libvirt/storage/default-pool
getfacl /var/run/libvirt/storage/default-pool/

getfacl: Removing leading '/' from absolute path names
# file: var/run/libvirt/storage/default-pool/
# owner: root
# group: libvirt
user::rwx
group::r-x
group:libvirt:rw-
mask::rwx
other::r-x
default:user::rwx
default:group::r-x
default:group:libvirt:rwx
default:mask::rwx
default:other::r-x
```

The above doesn't work, need to try some other options 

```
chmod g+rwxs /var/run/libvirt/storage/default-pool
```

Still getting a permission error and it seems this may be related to AppArmor and 
permissions.  Going to use QEMU session URL instead.

Trying that and it doesn't seem to be working and, looking at the provider code
I'm not convinced it's able to distinguish between system and session.

Looking at the code in [unix.go](https://github.com/dmacvicar/terraform-provider-libvirt/blob/8d7c58f67508ee9a958a6de98a722818edc75654/libvirt/uri/unix.go) for UNIX sockets the address is the path to the system socket.  It looks like
the host is stripped out of the URI [here](https://github.com/dmacvicar/terraform-provider-libvirt/blob/8d7c58f67508ee9a958a6de98a722818edc75654/libvirt/uri/connection_uri.go#L43) and only a query parameter `name` can be used.

Trying another tack and creating the default storage pool under my home directory

```
virsh pool-define-as --name default --type dir --target $HOME/.libvirt/storage/default-pool
```

Now get this error

```
libvirt_volume.debian12-qcow2: Creating...
libvirt_volume.debian12-qcow2: Creation complete after 7s [id=/home/fiona/.libvirt/storage/default-pool/debian12.qcow2]
libvirt_domain.debian12: Creating...
╷
│ Error: error creating libvirt domain: Cannot access storage file '/home/fiona/.libvirt/storage/default-pool/debian12.qcow2' (as uid:64055, gid:109): Permission denied
│ 
│   with libvirt_domain.debian12,
│   on main.tf line 11, in resource "libvirt_domain" "debian12":
│   11: resource "libvirt_domain" "debian12" {
│ 
╵
```

Seems that there is a `/etc/passwd` entry 

```
cat /etc/passwd | grep 64055

libvirt-qemu:x:64055:109:Libvirt Qemu,,,:/var/lib/libvirt:/usr/sbin/nologin
```


### Setting up logical volume

Checking physical volumes using `lsblk` which on my hardware shows I have a 1Tb drive on `/dev/sda`

Creating a volume group 

```
sudo vgcreate vgdata /dev/sda
sudo lvcreate -L 500G --name libvirt-pool vgdata
sudo mkfs -t ext4 /dev/vgdata/libvirt-pool 

# Need to add and entry for this to /etc/fstab to make it permenent
sudo mount /dev/vgdata/libvirt-pool /mnt/vms 

sudo mkdir -p /mnt/vms/libvirt/storage/pools/default
sudo chown libvirt-qemu:kvm -R /mnt/vms/libvirt
sudo chmod g+rwxs /mnt/vms/libvirt/storage/pools/default
```

Running `virsh` to create the pool

```
virsh pool-define-as --name default --type dir --target /mnt/vms/libvirt/storage/pools/default
```

Trying to create the VM again and still getting a permission error :(

In the end I edited the file `/etc/apparmor.d/local/abstractions/libvirt-qemu`

```
/mnt/vms/libvirt/** rwk,

```

Now I get a different error when trying to create the VM

```
libvirt_volume.debian12-qcow2: Creating...
libvirt_volume.debian12-qcow2: Creation complete after 6s [id=/mnt/vms/libvirt/storage/pools/default/debian12.qcow2]
libvirt_domain.debian12: Creating...
libvirt_domain.debian12: Creation complete after 1s [id=fa128e7e-4f7a-4fab-aac7-ce6ca5696843]
╷
│ Error: Invalid index
│ 
│   on main.tf line 39, in output "ip":
│   39:   value = "${libvirt_domain.debian12.network_interface.0.addresses.0}"
│     ├────────────────
│     │ libvirt_domain.debian12.network_interface[0].addresses is empty list of string
│ 
│ The given key does not identify an element in this collection value: the collection has no elements.
╵
```

It seems that the IP address isn't available initially fixed by using `wait_for_lease` option

```
  network_interface {
    network_name = "default" # List networks with virsh net-list
    wait_for_lease = true
  }
```


### Trying to get resizing to work

Added some debug to the cloud-init image

```
  user_data = <<EOF
#cloud-config
disable_root: 0
growpart:
  mode: auto
  devices: ['/']
write_files:
- content: |
    I was here!
  path: /etc/ci_debug_test
  permissions: '0644'
  owner: root:root
EOF
```

Running `tofu apply` got the following output

```
libvirt_cloudinit_disk.cloudinit_debian12_resized: Creating...
libvirt_volume.debian12-qcow2-base: Creating...
libvirt_volume.debian12-qcow2-base: Creation complete after 7s [id=/mnt/vms/libvirt/storage/pools/default/debian12.qcow2]
libvirt_cloudinit_disk.cloudinit_debian12_resized: Creation complete after 7s [id=/mnt/vms/libvirt/storage/pools/default/cloudinit_debian12_resized.iso;516c04a5-5187-451c-b89e-7ce8fcbf9810]
libvirt_volume.debian12-qcow2-resized: Creating...
libvirt_volume.debian12-qcow2-resized: Creation complete after 0s [id=/mnt/vms/libvirt/storage/pools/default/debian12-resized.qcow2]
libvirt_domain.debian12: Creating...
libvirt_domain.debian12: Still creating... [10s elapsed]
libvirt_domain.debian12: Creation complete after 16s [id=95e6793c-d8f6-46eb-bf16-09bcca3611eb]
```

Just realised that it's probably failing because it's the wrong image!


### Trying to get genericcloud image working

Using [this image](https://cloud.debian.org/images/cloud/bookworm/20231013-1532/debian-12-genericcloud-amd64-20231013-1532.qcow2) instead.
Also using `yamlencode` for the cloud-init meta data (user-data value) now as 
this makes setting up the data easier - can add multiple SSH keys.

Still doesn't seem to be working as it's not setting up the users and
SSH keys for root.

Shutting down the VM - can't connect to VM disk while it's running

```
virsh shutdown debian12
```
Running the following commands (as root) to connect to the VM 

```
modprobe nbd max_part=8
qemu-nbd --connect=/dev/nbd0 /mnt/vms/libvirt/storage/pools/default/debian12-resized.qcow2 

# Check partitions
fdisk -l /dev/nbd0

# Creating mount point
mkdir /mnt/qcowtmp

# Mount root partition
mount /dev/nbd0p1 /mnt/qcowtmp
```

It does look like the resizing may have been done as per the cloud init as 
the size of the root partition is 59Gb and I'd set it to `64 x 10^9` which 
does approximate to this.

There are log files for cloud init under `/var/log/cloud-init.log` and
`/var/log/cloud-init-output.log` with the following interesting sections

* Looks like the `user-data` file was read

```
2023-11-22 21:42:15,966 - util.py[DEBUG]: Reading from /run/cloud-init/tmp/tmpcsb10ajn//user-data (quiet=False)
2023-11-22 21:42:15,967 - util.py[DEBUG]: Read 1755 bytes from /run/cloud-init/tmp/tmpcsb10ajn//user-data
```

* Seems to be creating new new files

```
2023-11-22 21:42:15,974 - util.py[DEBUG]: Writing to /run/cloud-init/cloud-id-nocloud - wb: [644] 8 bytes
2023-11-22 21:42:15,974 - util.py[DEBUG]: Creating symbolic link from '/run/cloud-init/cloud-id' => '/run/cloud-init/cloud-id-nocloud'
2023-11-22 21:42:15,974 - atomic_helper.py[DEBUG]: Atomically writing to file /run/cloud-init/instance-data-sensitive.json (via temporary file /run/cloud-init/tmpejx8ywf2) - w: [600] 4735 bytes/chars
2023-11-22 21:42:15,974 - atomic_helper.py[DEBUG]: Atomically writing to file /run/cloud-init/instance-data.json (via temporary file /run/cloud-init/tmpxy4ijdsa) - w: [644] 1421 bytes/chars
2023-11-22 21:42:15,974 - handlers.py[DEBUG]: finish: init-local/search-NoCloud: SUCCESS: found local data from DataSourceNoCloud
2023-11-22 21:42:15,974 - stages.py[INFO]: Loaded datasource DataSourceNoCloud - DataSourceNoCloud [seed=/dev/sr0][dsmode=net]
```

  * Seems to load alot from `/etc`
  
```
2023-11-22 21:42:15,974 - util.py[DEBUG]: Reading from /etc/cloud/cloud.cfg (quiet=False)
2023-11-22 21:42:15,975 - util.py[DEBUG]: Read 2796 bytes from /etc/cloud/cloud.cfg
2023-11-22 21:42:15,975 - util.py[DEBUG]: Attempting to load yaml from string of length 2796 with allowed root types (<class 'dict'>,)
2023-11-22 21:42:15,978 - util.py[DEBUG]: Reading from /etc/cloud/cloud.cfg.d/05_logging.cfg (quiet=False)
2023-11-22 21:42:15,978 - util.py[DEBUG]: Read 2070 bytes from /etc/cloud/cloud.cfg.d/05_logging.cfg
2023-11-22 21:42:15,978 - util.py[DEBUG]: Attempting to load yaml from string of length 2070 with allowed root types (<class 'dict'>,)
2023-11-22 21:42:15,979 - util.py[DEBUG]: Reading from /etc/cloud/cloud.cfg.d/01_debian_cloud.cfg (quiet=False)
2023-11-22 21:42:15,979 - util.py[DEBUG]: Read 309 bytes from /etc/cloud/cloud.cfg.d/01_debian_cloud.cfg
2023-11-22 21:42:15,979 - util.py[DEBUG]: Attempting to load yaml from string of length 309 with allowed root types (<class 'dict'>,)
2023-11-22 21:42:15,980 - util.py[DEBUG]: Reading from /run/cloud-init/cloud.cfg (quiet=False)
2023-11-22 21:42:15,980 - util.py[DEBUG]: Read 35 bytes from /run/cloud-init/cloud.cfg
2023-11-22 21:42:15,980 - util.py[DEBUG]: Attempting to load yaml from string of length 35 with allowed root types (<class 'dict'>,)
2023-11-22 21:42:15,980 - util.py[DEBUG]: Attempting to load yaml from string of length 0 with allowed root types (<class 'dict'>,)
2023-11-22 21:42:15,980 - util.py[DEBUG]: loaded blob returned None, returning default.
2023-11-22 21:42:15,980 - util.py[DEBUG]: Attempting to remove /var/lib/cloud/instance
2023-11-22 21:42:15,980 - util.py[DEBUG]: Creating symbolic link from '/var/lib/cloud/instance' => '/var/lib/cloud/instances/nocloud'
2023-11-22 21:42:15,981 - util.py[DEBUG]: Reading from /var/lib/cloud/instances/nocloud/datasource (quiet=False)
2023-11-22 21:42:15,981 - util.py[DEBUG]: Writing to /var/lib/cloud/instances/nocloud/datasource - wb: [644] 65 bytes
2023-11-22 21:42:15,981 - util.py[DEBUG]: Writing to /var/lib/cloud/data/previous-datasource - wb: [644] 65 bytes
2023-11-22 21:42:15,981 - util.py[DEBUG]: Reading from /var/lib/cloud/data/instance-id (quiet=False)
2023-11-22 21:42:15,981 - stages.py[DEBUG]: previous iid found to be NO_PREVIOUS_INSTANCE_ID
2023-11-22 21:42:15,981 - util.py[DEBUG]: Writing to /var/lib/cloud/data/instance-id - wb: [644] 8 bytes
2023-11-22 21:42:15,981 - util.py[DEBUG]: Writing to /run/cloud-init/.instance-id - wb: [644] 8 bytes
2023-11-22 21:42:15,981 - util.py[DEBUG]: Writing to /var/lib/cloud/data/previous-instance-id - wb: [644] 24 bytes
2023-11-22 21:42:15,982 - util.py[DEBUG]: Writing to /var/lib/cloud/instance/obj.pkl - wb: [400] 6121 bytes
2023-11-22 21:42:15,982 - main.py[DEBUG]: [local] init will now be targeting instance id: nocloud. new=True
2023-11-22 21:42:15,982 - util.py[DEBUG]: Reading from /etc/cloud/cloud.cfg (quiet=False)
2023-11-22 21:42:15,982 - util.py[DEBUG]: Read 2796 bytes from /etc/cloud/cloud.cfg
```


After MUCH hacking around I discovered the main problem was missing this 
from the beginning of the file:

```
#cloud-config
```

The cloud-init system will ignore the file completely without this line.
Apparently you can also use `#!/bin/sh` instead for scripts rather than 
using the YAML file.

Fixed this issue and all is now apparently working.

