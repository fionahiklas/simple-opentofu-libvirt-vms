# Defining VM Volume
resource "libvirt_volume" "debian12-qcow2-base" {
  name = "debian12.qcow2"
  pool = "default" # List storage pools using virsh pool-list
  source = "https://cloud.debian.org/images/cloud/bookworm/20231013-1532/debian-12-genericcloud-amd64-20231013-1532.qcow2"
  format = "qcow2"
}

resource "libvirt_volume" "debian12-qcow2-resized" {
  name = "debian12-resized.qcow2"
  pool = "default"
  base_volume_id = libvirt_volume.debian12-qcow2-base.id
  size = 64000000000 
}


# Use CloudInit to resize partitions
resource "libvirt_cloudinit_disk" "cloudinit_debian12_resized" {
  name = "cloudinit_debian12_resized.iso"
  pool = "default"

  user_data = templatefile("cloud-init.tftpl", 
    { ssh_keys = [ for keyFile in fileset("./ssh_keys", "*.pub"): file("./ssh_keys/${keyFile}") ] })
}

# Define KVM domain to create
resource "libvirt_domain" "debian12" {
  name   = "debian12"
  memory = "4096"
  vcpu   = 3
  cloudinit = libvirt_cloudinit_disk.cloudinit_debian12_resized.id

  network_interface {
    network_name = "default" # List networks with virsh net-list
    wait_for_lease = true
  }

  disk {
    volume_id = "${libvirt_volume.debian12-qcow2-resized.id}"
  }

  console {
    type = "pty"
    target_type = "serial"
    target_port = "0"
  }

  graphics {
    type = "spice"
    listen_type = "address"
    autoport = true
  }
}

# Output Server IP
output "ip" {
  value = "${libvirt_domain.debian12.network_interface.0.addresses.0}"
}
